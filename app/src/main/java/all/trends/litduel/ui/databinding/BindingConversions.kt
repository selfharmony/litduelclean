package all.trends.litduel.ui.databinding

import all.trends.litduel.data.models.Novel
import all.trends.litduel.ui.duel.DuelFragment
import all.trends.litduel.ui.duel.ReadNovelViewPagerAdapter
import android.databinding.BindingAdapter
import android.graphics.drawable.Drawable
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso


class BindingConversions {
    companion object {
        @JvmStatic @BindingAdapter("dynamicText")
        fun bindText(textView: TextView, value: ObservableString) {
            textView.text = value.get()
        }

        @JvmStatic
        @BindingAdapter("android:src", "app:error")
        fun loadImage(view: ImageView, url: String, error: Drawable) {
            if (!url.isEmpty()) {
                Picasso.with(view.context)
                        .load(url)
                        .error(error)
                        .into(view)
            }
        }

        @JvmStatic
        @BindingAdapter("app:authors")
        fun loadAuthors(view: TextView, novels: List<Novel>) {
            val text : StringBuilder = StringBuilder("Авторы: ")
            if (!novels.isEmpty()) {
                novels
                        .filter { novel -> !novel.author.isEmpty() }
                        .onEach { novel ->
                            if (novels.indexOf(novel) != novels.lastIndex)
                                text.append(novel.author + ", ")
                            else text.append(novel.author)
                        }
                view.text = text
            }
        }

        @JvmStatic
        @BindingAdapter("bind:handler")
        fun bindViewPagerAdapter(view: ViewPager, fragment: DuelFragment) {
            val adapter = ReadNovelViewPagerAdapter(fragment.activity.supportFragmentManager, linkedMapOf())
            view.adapter = adapter
        }

        @JvmStatic
        @BindingAdapter("bind:pager")
        fun bindViewPagerTabs(view: TabLayout, pagerView: ViewPager) {
            view.setupWithViewPager(pagerView, true)
        }
    }


}

