package all.trends.litduel.ui.novel

import all.trends.litduel.data.models.Novel
import all.trends.litduel.databinding.FragmentNovelLayoutBinding
import all.trends.litduel.ui.BaseFragment
import all.trends.litduel.utils.Utils
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

class NovelFragment : BaseFragment(), Contract.view {
    lateinit var novel: Novel

    override fun onCreate(savedInstanceState: Bundle?) {
        novel = arguments.getSerializable(NOVEL_KEY) as Novel
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val binding = FragmentNovelLayoutBinding.inflate(inflater, container, false)
        binding.model = Utils.novel2
        return binding.root
    }

    companion object {
        val TAG = "NovelFragment"
        private val NOVEL_KEY = "NovelKey"

        fun newInstance(novel: Novel): NovelFragment {
            val args = Bundle()
            args.putSerializable(NOVEL_KEY, novel)
            val fragment = NovelFragment()
            fragment.arguments = args
            return fragment
        }
    }
}