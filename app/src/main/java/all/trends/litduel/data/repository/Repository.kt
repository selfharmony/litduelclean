package all.trends.litduel.data.repository

import all.trends.litduel.data.models.Duel
import all.trends.litduel.data.models.DuelsListEntry
import io.reactivex.Flowable

interface Repository{

    fun subscribeDuelsList() : Flowable<DuelsListEntry>
    fun setDuel(duel: String)
    fun subscribeDuel(key: String): Flowable<Duel>
}