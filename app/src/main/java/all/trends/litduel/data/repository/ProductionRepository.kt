package all.trends.litduel.data.repository

import all.trends.litduel.data.api.RealTimeDatabase
import all.trends.litduel.data.models.Duel
import all.trends.litduel.data.models.DuelsListEntry
import all.trends.litduel.data.storage.LocalStorage
import io.reactivex.Flowable

class ProductionRepository(val database: RealTimeDatabase,
                           val localStorage: LocalStorage) : Repository {

    override fun subscribeDuelsList(): Flowable<DuelsListEntry> = database.subscribeForDuels()

    override fun subscribeDuel(key: String): Flowable<Duel> = database.subscribeForSingleDuel(key)

    override fun setDuel(duel: String) {
        database.setDuel(duel)
    }

}