/*
package all.trends.litduel.domain.mapper

import com.lunchdot.restaurant.data.dto.response.Constructor
import com.lunchdot.restaurant.data.dto.response.Item
import com.lunchdot.restaurant.data.dto.response.OrderByIdResponse
import com.lunchdot.restaurant.ui.objects.OrderInfo
import io.reactivex.functions.Function
import java.lang.StringBuilder

class OrderInfoMapper : Function<OrderByIdResponse, OrderInfo> {
    override fun apply(orderById: OrderByIdResponse?): OrderInfo {
        return OrderInfo(
                orderById?.id,
                orderById?.time,
                orderById?.personCount,
                itemsToSting(orderById?.items),
                constructorsToString(orderById?.constructors),
                orderById?.status
        )
    }

    private fun constructorsToString(constructors: List<Constructor>?): String? {
        val sb: StringBuilder = StringBuilder()
        constructors?.forEach { constructor ->
            sb.append(constructor.toString() + ":\n")
            constructor.steps?.forEach { step ->
                sb.append(step.toString() + ":\n")
                step.items?.forEach { item ->
                    sb.append(item.toString() + "\n")
                }
            }
        }
        return sb.toString()
    }

    private fun itemsToSting(items: List<Item>?): String? {
        val sb: StringBuilder = StringBuilder()
        items?.forEach { item ->
            sb.append(item.toString() + "\n")
        }
        return sb.toString()
    }

}*/
