package all.trends.litduel.ui.duels_list

import all.trends.litduel.data.models.DuelsListEntry
import all.trends.litduel.domain.interactors.Interactor
import all.trends.litduel.ui.BasePresenter
import android.util.Log
import io.reactivex.subscribers.DisposableSubscriber

class DuelsListPresenter(val subscribeDatabaseDuelsListInteractor: Interactor<Void, DuelsListEntry>) : BasePresenter<Contract.view>(), Contract.presenter {
    override fun onStart() {

        subscribeDatabaseDuelsListInteractor.execute(object : DisposableSubscriber<DuelsListEntry>(){
            override fun onNext(duelsListEntry: DuelsListEntry) {
                duelsListEntry
                        .let {
                            when {
                                it.duel != null -> view?.loadDuel(it)
                                else -> view?.removeDuel(it.key)
                            }
                        }
            }

            override fun onError(t: Throwable?) {
                Log.e("FIREBASE", t?.message)
            }

            override fun onComplete() {}
        })
    }


    override fun onStop() {

    }


}