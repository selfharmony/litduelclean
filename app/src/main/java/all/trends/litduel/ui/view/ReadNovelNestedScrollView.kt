package all.trends.litduel.ui.view

import all.trends.litduel.R
import android.content.Context
import android.support.v4.widget.NestedScrollView
import android.util.AttributeSet
import android.view.LayoutInflater

//created by power on 09.11.17

class ReadNovelNestedScrollView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : NestedScrollView(context, attrs, defStyleAttr) {
    init {
        LayoutInflater.from(context).inflate(R.layout.view_read_novel, this, true)
    }
}