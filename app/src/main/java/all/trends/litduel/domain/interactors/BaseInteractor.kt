package all.trends.litduel.domain.interactors

import all.trends.litduel.data.repository.Repository
import io.reactivex.Flowable
import io.reactivex.Scheduler
import io.reactivex.disposables.Disposable
import io.reactivex.disposables.Disposables
import io.reactivex.schedulers.Schedulers
import io.reactivex.subscribers.DisposableSubscriber
import java.util.concurrent.Executor

abstract class BaseInteractor<T, R>(val threadScheduler: Executor, val uiScheduler: Scheduler, val repo: Repository) : Interactor<T, R> {
    protected var parameter: T? = null

    private var disposable: Disposable = Disposables.empty()

    abstract fun buildObservable(): Flowable<R>

    override fun execute(subscriber: DisposableSubscriber<R>) {
        disposable = subscriber
        buildObservable()
                .subscribeOn(Schedulers.from(threadScheduler))
                .observeOn(uiScheduler)
                .subscribe(subscriber)
    }


    override fun updateParameter(parameter: T) {
        this.parameter = parameter
    }

    override fun dispose() {
        disposable.dispose()
    }

    override fun getObservable(): Flowable<R> {
        return buildObservable().share()
                .subscribeOn(Schedulers.from(threadScheduler))
                .observeOn(uiScheduler)
    }

    override fun isDisposed(): Boolean {
        return disposable.isDisposed
    }


}