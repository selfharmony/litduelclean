package all.trends.litduel.ui.duel

import all.trends.litduel.data.models.Duel
import all.trends.litduel.domain.interactors.Interactor
import all.trends.litduel.ui.BasePresenter
import android.util.Log
import io.reactivex.subscribers.DisposableSubscriber

class DuelPresenter(val subscribeDuelInteractor: Interactor<String, Duel>) : BasePresenter<Contract.view>(), Contract.presenter {

    override fun onStart() {}
    override fun onStop() {
        subscribeDuelInteractor.dispose()
    }

    override fun subscribeForDuel(key: String) {
        subscribeDuelInteractor.updateParameter(key)
        view?.showLoading()
        subscribeDuelInteractor.execute(object : DisposableSubscriber<Duel>(){
            override fun onComplete() {}

            override fun onNext(duel: Duel?) {
                duel.let { view?.updateDuel(it) }
//                duel?.novels?.mapNotNull { novel -> view?.updateAuthorInTabs(novel.author, duel.novels.indexOf(novel)) }
            }

            override fun onError(t: Throwable?) {
                Log.e("single duel error", t?.localizedMessage)
            }
        })
    }

}