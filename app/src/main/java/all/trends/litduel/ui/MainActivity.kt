package all.trends.litduel.ui


import all.trends.litduel.R
import all.trends.litduel.ui.duel.DuelFragment
import all.trends.litduel.ui.duels_list.DuelsListFragment
import all.trends.litduel.ui.novel.NovelFragment
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.widget.Toast
import com.firebase.ui.auth.AuthUI
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper


class MainActivity : AppCompatActivity(), Router, FragmentsUiManager, NavigationView.OnNavigationItemSelectedListener {


    private val TAG = "MainActivity"
    private val RC_SIGN_IN = 9001

    val fm: FragmentManager by lazy { supportFragmentManager }
    private val mFirebaseAuth: FirebaseAuth by lazy { FirebaseAuth.getInstance(); }
    lateinit var mAuthStateListener: FirebaseAuth.AuthStateListener



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initToolbarAndDrawer()
        fm.addOnBackStackChangedListener {
            val topFragment = fm.findFragmentById(R.id.main_container)
            when (topFragment) {
                is BaseFragment -> {
                    topFragment.view?.requestFocus()
                    isFullScreen(topFragment.isFullScreen())
                }
            }
        }
//        mFirebaseAuth.signOut()
        mAuthStateListener = initAuthListener()
    }

    private fun initToolbarAndDrawer() {
        val toolbar = toolbar
        setSupportActionBar(toolbar)
        val drawer = drawer_layout
        val toggle = ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer.addDrawerListener(toggle)
        toggle.syncState()

        val navigationView = nav_view
        navigationView.setNavigationItemSelectedListener(this)
    }

    private fun initAuthListener(): FirebaseAuth.AuthStateListener {
        return FirebaseAuth.AuthStateListener {
            val user = mFirebaseAuth.currentUser
            if (user != null) {
                navigateToDuels()
            }
            else {
                startActivityForResult(AuthUI
                        .getInstance()
                        .createSignInIntentBuilder()
                        .setIsSmartLockEnabled(false)
                        .setAvailableProviders(mutableListOf(
                                AuthUI.IdpConfig.Builder(AuthUI.EMAIL_PROVIDER).build(),
                                AuthUI.IdpConfig.Builder(AuthUI.GOOGLE_PROVIDER).build()))
                        .build(), RC_SIGN_IN)
            }
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val drawer = drawer_layout
        drawer.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onBackPressed() {
        onBackPressedInTopFragment()
        super.onBackPressed()
        closeActivityIfNoFragment()
    }

    private fun onBackPressedInTopFragment() {
        val topFragment = fm.findFragmentById(R.id.main_container)
        (topFragment as? BaseFragment)?.onBackPressedCallback()
    }

    private fun closeActivityIfNoFragment() {
        val topFragment = fm.findFragmentById(R.id.main_container)
        if (topFragment == null)
            finish()
    }

    private fun replaceTransaction(fragment: Fragment): FragmentTransaction {
        val transaction = fm.beginTransaction()
        val topFragment = fm.findFragmentById(R.id.main_container)
        if (topFragment?.javaClass == fragment.javaClass) {
            return transaction
        } else {
//            fm.popBackStack()
            transaction.replace(R.id.main_container, fragment)
            return transaction
        }
    }

    private fun addTransaction(fragment: Fragment): FragmentTransaction {
        return fm.beginTransaction()
                .add(R.id.main_container, fragment)
                .addToBackStack(null)
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

    override fun onResume() {
        super.onResume()
        mFirebaseAuth.addAuthStateListener(mAuthStateListener)
    }

    override fun onPause() {
        super.onPause()
        mFirebaseAuth.removeAuthStateListener(mAuthStateListener)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(this, getString(R.string.auth_success), Toast.LENGTH_SHORT).show()
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, getString(R.string.auth_cancelled), Toast.LENGTH_SHORT).show()
                finish()
            }
        }
    }

    //#UiManager

    override fun isFullScreen(fullScreenMode: Boolean) {
        if (fullScreenMode) {
            supportActionBar?.hide()
        } else {
//            setSupportActionBar(mainToolbar)
            supportActionBar?.show()
        }
    }

    override fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun showLoading() {}

    override fun hideLoading() {}

    //#Router

    override fun navigateToDuel(key: String) {
        addTransaction(DuelFragment.newInstance(key)).commit()
    }

    override fun navigateToDuels() {
        addTransaction(DuelsListFragment.newInstance()).commit()
    }

    override fun navigateToNovel() {
        addTransaction(NovelFragment()).commit()
    }
}

interface Router{
    fun navigateToDuel(key: String)
    fun navigateToNovel()
    fun navigateToDuels()
}

interface FragmentsUiManager {
    fun showToast(message: String)
    fun isFullScreen(fullScreenMode: Boolean)
    fun showLoading()
    fun hideLoading()
//    fun showOrderDialog(message: String)
}