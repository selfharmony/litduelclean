package all.trends.litduel.domain.interactors

import all.trends.litduel.data.models.Duel
import all.trends.litduel.data.models.DuelsListEntry
import all.trends.litduel.data.repository.Repository
import io.reactivex.Flowable
import io.reactivex.Scheduler
import java.util.concurrent.Executor


open class SubscribeDatabaseDuelsListInteractor(threadScheduler: Executor, uiScheduler: Scheduler, repo: Repository)
    : BaseInteractor<Void, DuelsListEntry>(threadScheduler, uiScheduler, repo) {
    override fun buildObservable(): Flowable<DuelsListEntry> {
        return repo
                .subscribeDuelsList()
    }
}

open class SubscribeDuelInteractor(threadScheduler: Executor, uiScheduler: Scheduler, repo: Repository)
    : BaseInteractor<String, Duel>(threadScheduler, uiScheduler, repo) {
    override fun buildObservable(): Flowable<Duel> {
        return repo
                .subscribeDuel(parameter!!)
    }
}



