package all.trends.litduel.ui

abstract class BasePresenter<V : BaseContract.View> : BaseContract.Presenter {
    var view: V? = null

    fun dropView() {
        this.view = null
    }

    abstract fun onStart()

    abstract fun onStop()

    //#TODO : Внутренний контейнер для интеракторов
}
