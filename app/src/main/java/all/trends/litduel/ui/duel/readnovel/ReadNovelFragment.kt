package all.trends.litduel.ui.duel.readnovel

import all.trends.litduel.data.models.Novel
import all.trends.litduel.databinding.ViewReadNovelBinding
import all.trends.litduel.ui.BaseFragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

//created by power on 10.11.17

class ReadNovelFragment: BaseFragment() {
    lateinit var binding: ViewReadNovelBinding

    lateinit var novel: Novel

    override fun onCreate(savedInstanceState: Bundle?) {
        novel = arguments.getSerializable(NOVEL_KEY) as Novel
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = ViewReadNovelBinding.inflate(inflater, container, false)
        binding.novel = novel
        return binding.root
    }

    companion object {
        val TAG = "ReadNovelFragment"
        private val NOVEL_KEY = "NovelKey"

        fun newInstance(novel: Novel): ReadNovelFragment {
            val args = Bundle()
            args.putSerializable(NOVEL_KEY, novel)
            val fragment = ReadNovelFragment()
            fragment.arguments = args
            return fragment
        }
    }


}