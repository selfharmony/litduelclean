package all.trends.litduel.ui.novel

import all.trends.litduel.ui.BaseContract

interface Contract {
    interface view : BaseContract.View {

    }
    interface presenter : BaseContract.Presenter{

    }
}