//package all.trends.litduel.ui.duel
//
//import all.trends.litduel.data.models.AuthorItem
//import all.trends.litduel.databinding.ItemAuthorBinding
//import all.trends.litduel.ui.view.ReadNovelViewPager
//import android.databinding.DataBindingUtil
//import android.support.v4.view.ViewPager
//import android.support.v7.widget.RecyclerView
//import android.view.LayoutInflater
//import android.view.View
//import android.view.ViewGroup
//import kotlin.properties.Delegates
//
//class AuthorsAdapter(private val authors: LinkedHashMap<Int, AuthorItem>, private val pager: ReadNovelViewPager,
//                     private val onItemClickListener: AuthorsAdapter.OnItemClickListener)
//    : RecyclerView.Adapter<AuthorsAdapter.ViewHolder>() {
//
//    private var binding: ItemAuthorBinding by Delegates.notNull()
//
//    init {
//        setOnViewPagerPageChangedListener(pager)
//    }
//
//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
//        val inflater = LayoutInflater.from(parent.context)
//        binding = ItemAuthorBinding.inflate(inflater, parent, false)
//        return ViewHolder(binding.root)
//    }
//
//    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
//
//        holder.binding.item = authors[pos]
//        holder.binding.root.setOnClickListener({
//            authors.keys.forEach { key ->
//                setRecyclerViewItemClickListenerForViewPager(key, pager)
//                onItemClickListener.onClick(key, authors[key]!!)
//            }
//        })
//    }
//
//
//
//    private fun setRecyclerViewItemClickListenerForViewPager(key: Int, pager: ReadNovelViewPager) {
//            pager.setCurrentItem(key, true)
//    }
//
//    private fun setOnViewPagerPageChangedListener(pager: ReadNovelViewPager){
//        pager.addOnPageChangeListener(object: ViewPager.OnPageChangeListener {
//            override fun onPageScrollStateChanged(state: Int) { }
//            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) { }
//            override fun onPageSelected(position: Int) {
//            }
//        })
//    }
//
//
////    override fun getItemCount(): Int = novels.size
//
//    override fun getItemCount(): Int {
//        return authors.size
//    }
//
//    private fun checkPosition(position: Int): Int {
//        return position //% novels.size
//    }
//
//    fun updateAuthor(entry: AuthorItem, key: Int) {
//        entry.let { listEntry -> authors.put(key, entry) }
//        notifyDataSetChanged()
//    }
//
//
//    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
//        var binding: ItemAuthorBinding = DataBindingUtil.bind(itemView)
//    }
//
//    interface OnItemClickListener {
//        fun onClick(position: Int, authorItem: AuthorItem)
//    }
//
//    fun removeAuthor(key: Int) {
//        authors.remove(key)
//        notifyDataSetChanged()
//    }
//
//
//}
