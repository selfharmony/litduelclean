package all.trends.litduel.ui.databinding

import android.databinding.BaseObservable

class ObservableString(var value: String) : BaseObservable() {

    fun get() :String {
        return value
    }

    fun set(string: String) {
        value = string
        notifyChange()
    }

    fun isEmpny() = value.isEmpty()

    fun lenght() = value.length

    fun clear() {
        value = ""
    }
}