package all.trends.litduel.ui.duels_list

import all.trends.litduel.core.App
import all.trends.litduel.core.PerFragment
import all.trends.litduel.data.models.Duel
import all.trends.litduel.data.models.DuelsListEntry
import all.trends.litduel.data.repository.Repository
import all.trends.litduel.databinding.FragmentDuelsListBinding
import all.trends.litduel.domain.interactors.Interactor
import all.trends.litduel.domain.interactors.SubscribeDatabaseDuelsListInteractor
import all.trends.litduel.ui.BaseFragment
import all.trends.litduel.ui.Router
import all.trends.litduel.ui.duels_list.adapter.DuelsListAdapter
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dagger.Provides
import io.reactivex.Scheduler
import java.util.concurrent.Executor
import javax.inject.Inject


class DuelsListFragment : BaseFragment(), Contract.view, DuelsListAdapter.OnItemClickListener {

    @Inject lateinit var presenter : DuelsListPresenter
    lateinit var component: Component
    lateinit var binding: FragmentDuelsListBinding
    lateinit var adapter: DuelsListAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inject()
        presenter.view = this
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentDuelsListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAdapter()
    }

    private fun initAdapter() {
        adapter = DuelsListAdapter(linkedMapOf(), this)
        binding.duelsList.layoutManager = GridLayoutManager(activity, 2) as RecyclerView.LayoutManager?
        binding.duelsList.adapter = adapter
    }

    override fun onStart() {
        super.onStart()
        presenter.onStart()
    }

    //region Dagger init
    fun inject() {
        val appComponent = (activity.application as App).appComponent
        component = DaggerDuelsListFragment_Component.builder().module(Module()).component(appComponent).build()
        component.inject(this)
    }

    @PerFragment
    @dagger.Component(
            modules = arrayOf(Module::class),
            dependencies = arrayOf(App.Component::class)
    )
    interface Component : App.Component {
        fun inject(activity: DuelsListFragment)
        fun presenter(): DuelsListPresenter
    }

    @dagger.Module
    class Module {
        @Provides
        @PerFragment
        fun provideDuelsListPresenter(subscribeDatabaseDuelsListInteractor: Interactor<Void, DuelsListEntry>): DuelsListPresenter {
            return DuelsListPresenter(subscribeDatabaseDuelsListInteractor)
        }

        @Provides
        @PerFragment
        fun provideSubscribeDatabaseDuelsListInteractor(executor: Executor, scheduler: Scheduler, repo: Repository): Interactor<Void, DuelsListEntry>{
            return SubscribeDatabaseDuelsListInteractor(executor, scheduler, repo)
        }
    }
    //endregion

    override fun showList(duels: MutableList<Duel>) {
    }


    override fun removeDuel(key: String) {
        adapter.removeDuel(key)
    }

    override fun loadDuel(duelsListEntry: DuelsListEntry) {
        adapter.addDuel(duelsListEntry)
        adapter.notifyDataSetChanged()
    }

    override fun onClick(key: String) {
        navigateToDuel(key)
    }

    private fun navigateToDuel(key: String) {
        (activity as Router).navigateToDuel(key)
    }

    override fun onStop() {
        super.onStop()
        presenter.onStop()
    }

//    override fun isFullScreen(): Boolean = true

    companion object {
        val TAG = "DuelsListFragment"

        fun newInstance(): DuelsListFragment {
            val fragment = DuelsListFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }
}