package all.trends.litduel.ui.duel

import all.trends.litduel.data.models.Novel
import all.trends.litduel.ui.duel.readnovel.ReadNovelFragment
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v7.widget.RecyclerView


//created by power on 09.11.17

class ReadNovelViewPagerAdapter(fm: FragmentManager, var novels: LinkedHashMap<Int, Novel>) : FragmentStatePagerAdapter(fm) {

    private var PAGES: Int = novels.size
    var authorsRecyclerView: RecyclerView? = null

    fun updateAdapter(newNovels: LinkedHashMap<Int, Novel>) {
        novels = newNovels
        PAGES = novels.size
    }

    fun updateNovel(key: Int, novel: Novel) {
        novels.put(key, novel)
        PAGES = novels.size

        notifyDataSetChanged()
    }

    override fun getItem(position: Int): Fragment {
        authorsRecyclerView?.scrollToPosition(position)
        return ReadNovelFragment.newInstance(novels.mapNotNull { it.value }[position])
    }


//    override fun isViewFromObject(view: View?, `object`: Any?): Boolean = view == `object` as ReadNovelNestedScrollView
//
//    override fun instantiateItem(collection: ViewGroup, position: Int): Any {
//        val novelScrollView = ReadNovelNestedScrollView(context)
//        novelScrollView.novelTextView.text = novels.entries.elementAt(position).value.text
//        return novelScrollView
//    }

    override fun getCount(): Int = PAGES


    fun associateAuthorsRecyclerView(recyclerView: RecyclerView?) {
        authorsRecyclerView = recyclerView
    }

}
