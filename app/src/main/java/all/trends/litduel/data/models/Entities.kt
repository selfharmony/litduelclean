package all.trends.litduel.data.models

import android.databinding.BaseObservable
import android.databinding.Bindable
import java.io.Serializable

//Created by selfharmony
class Duel : BaseObservable{

    constructor()
    constructor(theme: String, description: String, photo: String, novels: List<Novel>, finished: Boolean) {
        this.theme = theme
        this.description = description
        this.photo = photo
        this.novels = novels
        this.finished = finished
    }

    var theme: String = ""
    var description: String = ""
    var photo: String  = ""
    var novels: List<Novel> = mutableListOf()
    @Bindable var finished: Boolean = true
}

class Novel : Serializable{
    constructor()
    constructor(name: String, author: String, text: String)
    var name: String = ""
    var author: String = ""
    var text: String = ""
}


class DuelsListEntry(val key: String, val duel: Duel?)