package all.trends.litduel.utils

import android.content.Context
import android.support.design.widget.AppBarLayout
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.FloatingActionButton
import android.util.AttributeSet
import android.view.View




//created by power on 10.11.17

class ShrinkFabBehaviour(context: Context?, attrs: AttributeSet?) : CoordinatorLayout.Behavior<FloatingActionButton>(context, attrs) {

    override fun layoutDependsOn(parent: CoordinatorLayout, fab: FloatingActionButton, dependency: View): Boolean {
        return dependency is AppBarLayout
    }

    override fun onDependentViewChanged(parent: CoordinatorLayout?, fab: FloatingActionButton?, dependency: View?): Boolean {
        updateFabState(fab, getAppbarState(parent, fab))
        return false
    }

    private fun updateFabState(fab: FloatingActionButton?, appBarIsHidden: Boolean) =
            if (!appBarIsHidden) {
                fab?.hide()
            } else fab?.show()

    private fun getAppbarState(parent: CoordinatorLayout?, fab: FloatingActionButton?) : Boolean {
        val dependencies = parent?.getDependencies(fab.takeIf { it != null }!!)
        dependencies?.mapNotNull { view ->
            if (view is AppBarLayout) {
                val height = view.getHeight() - view.getBottom()
              return  height < view.getHeight()/2
            }
        }
        return false
    }

}