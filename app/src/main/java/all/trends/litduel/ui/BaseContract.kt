package all.trends.litduel.ui

interface BaseContract{
    interface View{
        fun showToast(message: String)
        fun showLoading()
        fun hideLoading()
    }

    interface Presenter{

    }
}