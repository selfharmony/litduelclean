package all.trends.litduel.ui.duels_list

import all.trends.litduel.data.models.Duel
import all.trends.litduel.data.models.DuelsListEntry
import all.trends.litduel.ui.BaseContract

interface Contract : BaseContract{
    interface view : BaseContract.View {
        fun loadDuel(duelsListEntry: DuelsListEntry)

        fun showList(duels: MutableList<Duel>)
        fun removeDuel(key: String)
    }
    interface presenter: BaseContract.Presenter{

    }
}