package all.trends.litduel.ui.duel

import all.trends.litduel.core.App
import all.trends.litduel.core.PerFragment
import all.trends.litduel.data.models.Duel
import all.trends.litduel.data.models.Novel
import all.trends.litduel.data.repository.Repository
import all.trends.litduel.databinding.FragmentReadDuelBinding
import all.trends.litduel.domain.interactors.Interactor
import all.trends.litduel.domain.interactors.SubscribeDuelInteractor
import all.trends.litduel.ui.BaseFragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dagger.Provides
import io.reactivex.Scheduler
import kotlinx.android.synthetic.main.fragment_read_duel.view.*
import java.util.concurrent.Executor
import javax.inject.Inject

class DuelFragment : BaseFragment(), Contract.view {


    @Inject lateinit var presenter : DuelPresenter
    lateinit var duelKey: String
    lateinit var component: Component
    lateinit var binding: FragmentReadDuelBinding
    lateinit var novelsAdapter: ReadNovelViewPagerAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inject()
        presenter.view = this
        duelKey = arguments.getString(DUEL_KEY, "")
    }

    //region Dagger init
    fun inject() {
        val appComponent = (activity.application as App).appComponent
        component = DaggerDuelFragment_Component.builder().module(Module()).component(appComponent).build()
        component.inject(this)
    }

    @PerFragment
    @dagger.Component(
            modules = arrayOf(Module::class),
            dependencies = arrayOf(App.Component::class)
    )
    interface Component : App.Component {
        fun inject(activity: DuelFragment)
        fun presenter(): DuelPresenter
    }

    @dagger.Module
    class Module {
        @Provides
        @PerFragment
        fun provideDuelPresenter(subscribeDuelInteractor: Interactor<String, Duel>): DuelPresenter {
            return DuelPresenter(subscribeDuelInteractor)
        }

        @Provides
        @PerFragment
        fun provideSubscribeDuelInteractor(executor: Executor, scheduler: Scheduler, repo: Repository): Interactor<String, Duel> {
            return SubscribeDuelInteractor(executor, scheduler, repo)
        }
    }
    //endregion

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentReadDuelBinding.inflate(inflater, container, false)
        binding.duel = Duel()
        binding.handler = this
        return binding.root
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.subscribeForDuel(duelKey)
    }


    override fun updateDuel(duel: Duel?) {
        binding.duel = duel
        duel?.novels?.map { novel ->
            updateNovelInList(novel, duel.novels.indexOf(novel))
        }
        duel?.novels?.map { novel ->
            updateAuthorInTabs(novel.author, duel.novels.indexOf(novel))
        }
        binding.notifyChange()
    }

    private fun updateNovelInList(novel: Novel, key: Int) {
        (binding.root.readNovelViewPager.adapter as ReadNovelViewPagerAdapter).updateNovel(key, novel)

    }

     private fun updateAuthorInTabs(author: String, key: Int) {
         val tab = binding.root.authorsTabLayout.getTabAt(key)
         tab?.text = author
    }

    override fun isFullScreen(): Boolean = true

    companion object {
        val TAG = "DuelFragment"
        private val DUEL_KEY = "DuelKey"

        fun newInstance(key: String): DuelFragment {
            val args = Bundle()
            args.putString(DUEL_KEY, key)
            val fragment = DuelFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onStop()
    }
}

