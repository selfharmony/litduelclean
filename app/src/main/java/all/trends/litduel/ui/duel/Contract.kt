package all.trends.litduel.ui.duel

import all.trends.litduel.data.models.Duel
import all.trends.litduel.ui.BaseContract

interface Contract : BaseContract{
    interface view : BaseContract.View{
        fun updateDuel(it: Duel?)

    }
    interface presenter : BaseContract.Presenter{

        fun subscribeForDuel(key: String)
    }
}