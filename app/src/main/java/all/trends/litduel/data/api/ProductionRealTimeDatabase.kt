package all.trends.litduel.data.api

import all.trends.litduel.data.models.Duel
import all.trends.litduel.data.models.DuelsListEntry
import com.google.firebase.database.*
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.subjects.PublishSubject


class ProductionRealTimeDatabase(val database: FirebaseDatabase) : RealTimeDatabase {

    private val messageSubject: PublishSubject<DuelsListEntry> = PublishSubject.create()
    private val singleDuelSubject: PublishSubject<Duel> = PublishSubject.create()

    private var duelsListReferense: DatabaseReference = database.getReference("duels")
    private fun singleDuelReference(key: String): DatabaseReference = database.getReference("duels").child(key)

    private fun initDuelsListListener() {
        duelsListReferense.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated
                dataSnapshot.children?.mapNotNull {
                    val value = DuelsListEntry(it.key, it.getValue(Duel::class.java))
                    messageSubject.onNext(value)
                }
            }

            override fun onCancelled(error: DatabaseError) {
                // Failed to read value
                messageSubject.onError(error.toException())
//                Log.w("onCancelled", "Failed to read value.", error.toException())
            }
        })

        duelsListReferense.addChildEventListener(object :ChildEventListener {
            override fun onCancelled(p0: DatabaseError?) {}
            override fun onChildMoved(p0: DataSnapshot?, p1: String?) {}
            override fun onChildChanged(dataSnapshot: DataSnapshot?, p1: String?) {}
            override fun onChildAdded(dataSnapshot: DataSnapshot?, p1: String?) {}

            override fun onChildRemoved(data: DataSnapshot?) {
                data.takeIf { it?.key != null}
                        .let { entry ->
                            val value = DuelsListEntry(entry?.key!!, null)
                            messageSubject.onNext(value)
                        }
            }
        })
    }

    private fun initSingleDuelListener(key: String) {
        val reference = singleDuelReference(key)
        reference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated
                dataSnapshot.let {
                    val duel = it.getValue(Duel::class.java)
                    singleDuelSubject.onNext(duel)
                }
            }

            override fun onCancelled(error: DatabaseError) {
                // Failed to read value
                singleDuelSubject.onError(error.toException())
            }
        })
    }

    override fun subscribeForSingleDuel(key: String): Flowable<Duel> {
        initSingleDuelListener(key)
        return singleDuelSubject.toFlowable(BackpressureStrategy.BUFFER)
    }



    override fun subscribeForDuels(): Flowable<DuelsListEntry> {
        initDuelsListListener()
        return messageSubject.toFlowable(BackpressureStrategy.BUFFER)
    }

    override fun setDuel(message: String) {
        duelsListReferense.setValue(message)
    }

    override fun unsubscribeDuels() {
//      messageSubject.dispo
    }
}