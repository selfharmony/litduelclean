package all.trends.litduel.core

import all.trends.litduel.R
import all.trends.litduel.data.repository.Repository
import all.trends.litduel.di.MainModule
import android.app.Application
import io.reactivex.Scheduler
import uk.co.chrisjenx.calligraphy.CalligraphyConfig
import java.util.concurrent.Executor
import javax.inject.Singleton



class App : Application(){
    var appComponent: Component? = null

    override fun onCreate() {
        super.onCreate()
        appComponent = createAppComponent()
        setDefaultFont()
    }


    private fun setDefaultFont() {
        CalligraphyConfig.initDefault(CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/lato/Lato-Light.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        )
    }

    private fun createAppComponent(): Component {
        return DaggerApp_Component.builder().mainModule((MainModule(this))).build()
    }

    @Singleton
    @dagger.Component(modules = arrayOf(MainModule::class))
    interface Component {
        fun repo(): Repository
        fun workerThread(): Executor
        fun uiThread(): Scheduler
    }
}