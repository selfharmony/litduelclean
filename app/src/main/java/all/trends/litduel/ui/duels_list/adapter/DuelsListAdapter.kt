package all.trends.litduel.ui.duels_list.adapter

import all.trends.litduel.data.models.Duel
import all.trends.litduel.data.models.DuelsListEntry
import all.trends.litduel.databinding.ItemDuelBinding
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlin.properties.Delegates

//created by power on 21.10.17

class DuelsListAdapter(private val duels: LinkedHashMap<String, Duel>,
                       private val onItemClickListener: DuelsListAdapter.OnItemClickListener)
    : RecyclerView.Adapter<DuelsListAdapter.ViewHolder>() {

    private var binding: ItemDuelBinding by Delegates.notNull()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        binding = ItemDuelBinding.inflate(inflater, parent, false)
        return ViewHolder(binding.root)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.duel = (duels.values.elementAt(position))
        holder.binding.root.setOnClickListener({
            duels.keys.elementAt(position)
                    .let { key ->  onItemClickListener.onClick(key)}})
    }


    override fun getItemCount(): Int = duels.size

    fun addDuel(entry: DuelsListEntry) {
        entry.let { listEntry -> duels.put(listEntry.key, listEntry.duel!!)}
    }

    fun changeDuel(key: String, duel: Duel) {
        duels[key] = duel
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var binding: ItemDuelBinding = DataBindingUtil.bind(itemView)
    }

    interface OnItemClickListener {
        fun onClick(key: String)
    }

    fun removeDuel(key: String) {
        duels.remove(key)
        notifyDataSetChanged()
    }


}