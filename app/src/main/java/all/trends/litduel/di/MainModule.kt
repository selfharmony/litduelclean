package all.trends.litduel.di

import all.trends.litduel.core.APP_PREFERENCES
import all.trends.litduel.data.api.ProductionRealTimeDatabase
import all.trends.litduel.data.api.RealTimeDatabase
import all.trends.litduel.data.repository.ProductionRepository
import all.trends.litduel.data.repository.Repository
import all.trends.litduel.data.storage.LocalStorage
import all.trends.litduel.data.storage.ProductionLocalStorage
import all.trends.litduel.domain.interactors.ThreadScheduler
import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.google.firebase.database.FirebaseDatabase
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import java.util.concurrent.Executor
import javax.inject.Singleton

@dagger.Module
class MainModule(val app: Application) {

    @Singleton
    @Provides
    fun provideWorkerThread(): Executor {
        return ThreadScheduler()
    }

    @Singleton
    @Provides
    fun provideUiThread(): Scheduler {
        return AndroidSchedulers.mainThread()
    }

    @Singleton
    @Provides
    fun provideRepository(database: RealTimeDatabase, localStorage: LocalStorage): Repository {
        return ProductionRepository(database, localStorage)
    }

    @Singleton
    @Provides
    fun provideRealTimeDatabase(firebaseDatabase: FirebaseDatabase): RealTimeDatabase{
        return ProductionRealTimeDatabase(firebaseDatabase)
    }

    @Singleton
    @Provides
    fun provideFirebaseDatabase(): FirebaseDatabase{
        return FirebaseDatabase.getInstance()
    }

    @Singleton
    @Provides
    fun provideLocalStorage(prefs: SharedPreferences): LocalStorage {
        return ProductionLocalStorage(prefs)
    }


    @Provides
    @Singleton
    fun providesSharedPreferences(): SharedPreferences {
        return app.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE)
    }

}