package all.trends.litduel.data.api

import all.trends.litduel.data.models.Duel
import all.trends.litduel.data.models.DuelsListEntry
import io.reactivex.Flowable

interface RealTimeDatabase {
    fun subscribeForDuels(): Flowable<DuelsListEntry>
    fun unsubscribeDuels()
    fun setDuel(message: String)
    fun subscribeForSingleDuel(key: String): Flowable<Duel>
}